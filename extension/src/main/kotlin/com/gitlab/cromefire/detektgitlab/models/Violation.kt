package com.gitlab.cromefire.detektgitlab.models

import kotlinx.serialization.Serializable

@Serializable
internal data class Violation(
    /**
     * A description of the code quality violation.
     */
    val description: String,
    /**
     * A unique fingerprint to identify the code quality violation. For example, an MD5 hash.
     */
    val fingerprint: String,
    /**
     * A severity string.
     */
    val severity: Severity,
    /**
     * Location of the violation.
     */
    val location: Location
)