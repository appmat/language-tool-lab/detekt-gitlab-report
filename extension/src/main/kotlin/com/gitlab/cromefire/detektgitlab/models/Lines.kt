package com.gitlab.cromefire.detektgitlab.models

import kotlinx.serialization.Serializable

@Serializable
internal data class Lines(
    /**
     * The line on which the code quality violation occurred.
     */
    val begin: Int
)
