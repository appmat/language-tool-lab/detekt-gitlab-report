package com.gitlab.cromefire.detektgitlab.models

import kotlinx.serialization.Serializable

@Serializable
internal data class Location(
    /**
     * The relative path to the file containing the code quality violation.
     */
    val path: String,
    /**
     * The line on which the code quality violation occurred.
     */
    val lines: Lines
)
