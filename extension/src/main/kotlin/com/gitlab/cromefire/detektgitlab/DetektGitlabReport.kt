package com.gitlab.cromefire.detektgitlab

import com.gitlab.cromefire.detektgitlab.models.Lines
import com.gitlab.cromefire.detektgitlab.models.Location
import com.gitlab.cromefire.detektgitlab.models.Severity
import com.gitlab.cromefire.detektgitlab.models.Violation
import io.gitlab.arturbosch.detekt.api.Detektion
import io.gitlab.arturbosch.detekt.api.OutputReport
import io.gitlab.arturbosch.detekt.api.SeverityLevel
import java.security.MessageDigest
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json

class DetektGitlabReport : OutputReport() {
    override val ending: String = "json"
    private val json by lazy {
        Json {}
    }

    override fun render(detektion: Detektion): String {
        val violations = detektion.findings.flatMap {
            it.value.map { finding ->
                val digest = MessageDigest.getInstance("MD5")
                Violation(
                    "${finding.id}: ${finding.messageOrDescription()}",
                    bytesToHex(digest.digest("${finding.id}|${finding.location.filePath.relativePath.toString()}|${finding.startPosition}".encodeToByteArray())),
                    mapSeverity(finding.severity),
                    Location(
                        finding.location.filePath.relativePath?.toString()
                            ?: throw IllegalStateException("Relative path not available."),
                        Lines(finding.startPosition.line)
                    )
                )
            }
        }
        return json.encodeToString(ListSerializer(Violation.serializer()), violations)
    }

    private companion object {
        fun mapSeverity(level: SeverityLevel): Severity = when (level) {
            // TODO: Make configurable
            SeverityLevel.ERROR -> Severity.Major
            SeverityLevel.WARNING -> Severity.Minor
            SeverityLevel.INFO -> Severity.Info
        }

        fun bytesToHex(hash: ByteArray): String {
            val hexString = StringBuilder(2 * hash.size)
            for (i in hash.indices) {
                val hex = Integer.toHexString(0xff and hash[i].toInt())
                if (hex.length == 1) {
                    hexString.append('0')
                }
                hexString.append(hex)
            }
            return hexString.toString()
        }
    }
}
