package com.gitlab.cromefire.detektgitlab.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal enum class Severity {
    @SerialName("info")
    Info,
    @SerialName("minor")
    Minor,
    @SerialName("major")
    Major,
    @SerialName("critical")
    Critical,
    @SerialName("blocker")
    Blocker,
}
