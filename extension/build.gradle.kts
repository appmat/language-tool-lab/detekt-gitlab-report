plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    `maven-publish`
}

val ciTag: String? = System.getenv("CI_COMMIT_TAG")

group = "com.gitlab.cromefire"
val snapshotVersion = "0.1.0"
version = if (!ciTag.isNullOrBlank() && ciTag.startsWith("v")) {
    ciTag.substring(1)
} else {
    "$snapshotVersion-SNAPSHOT"
}

repositories {
    mavenCentral()
    jcenter {
        content {
            includeModule("org.jetbrains.kotlinx", "kotlinx-html-jvm")
        }
    }
}

dependencies {
    implementation("io.gitlab.arturbosch.detekt:detekt-api:1.16.0")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.1.0")

    testImplementation("io.gitlab.arturbosch.detekt:detekt-test:1.16.0")
}

publishing {
    publications {
        register<MavenPublication>("maven") {
            artifactId = "detekt-gitlab-report"

            from(components["java"])

            pom {
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("cromefire_")
                        name.set("Cromefire_")
                        email.set("cromefire_@outlook.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com/cromefire_/detekt-gitlab-report.git")
                    developerConnection.set("scm:git:ssh://git@gitlab.com/cromefire_/detekt-gitlab-report.git")
                    url.set("https://gitlab.com/cromefire_/detekt-gitlab-report")
                }
            }
        }
    }
    repositories {
        maven("https://gitlab.com/api/v4/projects/25796063/packages/maven") {
            name = "gitlab"

            credentials(HttpHeaderCredentials::class.java) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN") ?: ""
            }
            authentication {
                register("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}
